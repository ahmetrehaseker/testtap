# Documentation: http://docs.brew.sh/Formula-Cookbook.html
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!

class Createdataresource < Formula
  desc ""
  homepage ""
  url "https://s3-us-west-2.amazonaws.com/og-downloads-sandbox-oregon/resource1.0.tar.gz"
  sha256 "c7e1a3e007a922bf79407b9f97daa1ebd5262615a47aebd86056e67bc00b9489"

  # depends_on "cmake" => :build

  def install
    rm Dir["bin/*.{bat,cmd,dll,exe}"]
    libexec.install Dir["*"]
    bin.install_symlink Dir["#{libexec}/*"]
  end
end

